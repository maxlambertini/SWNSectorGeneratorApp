/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';

class SWNWorldDataWidget extends StatelessWidget {
  final SWNWorld world;
  SWNWorldDataWidget(this.world);
  final TextStyle boldStyle = new TextStyle( fontWeight: FontWeight.bold);

  Widget _buildRow (label, value) {
    return new Container(
      child: new Column (
        mainAxisAlignment:  MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          new Text(label, style: boldStyle),
          new Text(value),          
        ],
      ),
    );
  }

  @override
  Widget build (BuildContext context) {
    return new Container (
      padding: new EdgeInsets.only(left: 16.0, right: 16.0, top: 16.0, bottom: 10.0),
      child: new Column(
        mainAxisAlignment:  MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          _buildRow("Atmosphere: ", world.atmosphere),
          _buildRow("Biosphere: ", world.biosphere),
          _buildRow("Climate: ", world.climate),
          _buildRow("Population: ", world.population),
          _buildRow("Technology: ", world.technology),
        ],
      )
    );
  }  
}


