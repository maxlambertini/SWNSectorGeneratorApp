import 'package:flutter/material.dart';
import '../swn.dart';
import 'section_items_widget.dart';

class SWNAnimalWidget extends StatelessWidget {
  SWNAnimal animal;
  SWNAnimalWidget(this.animal):super();

  @override
  Widget build(BuildContext context) {
      // TODO: implement build
      List<SectionItem> items = [];
      items.add(new SectionItem("Style", animal.animalStyle.toString()));
      items.add(new SectionItem("Appearance", animal.appearance.toString()));
      items.add(new SectionItem("Predator behavior", animal.predatorBehavior));
      items.add(new SectionItem("Prey behavior", animal.preyBehavior));
      items.add(new SectionItem("Scavenger behavior", animal.scavengerBehavior));
      if (animal.animalPoison != null)  
        items.add(new SectionItem("Poison details", animal.animalPoison.toString()));
      if (animal.harmfulDischarge != null)  
        items.add(new SectionItem("Discharge details", animal.harmfulDischarge));

      return new SectionItemWidget(
        items, 
        title: animal.name[0].toUpperCase()+ animal.name.substring(1),
        useDivider: true,
        showTitle: true, 
      );  
    }
}

