/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';
import 'swn_dialogs.dart';
import 'swn_situation.dart';
import 'swn_common_data.dart' as Common;
import 'swn_tag_widget.dart';
import 'swn_world_data_widget.dart';
import 'swn_animal_widget.dart';
import 'swn_animal_list_widget.dart';

typedef void SWNWorldCallback(SWNWorld world);



class SWNSingleWorldWidget extends StatefulWidget {
  final SWNWorld world;
  final SWNWorldCallback onRemoveButtonPressed;
  final bool showRemove;
  SWNSingleWorldWidget({this.world, this.onRemoveButtonPressed, this.showRemove});
  @override
  createState() => new SWNSingleWorldWidgetState();
}

const int MODE_SIMPLE_INFO = 0;
const int MODE_TAGS = 1;
const int MODE_ANIMALS = 2;

class SWNSingleWorldWidgetState extends State<SWNSingleWorldWidget> {
  int showMode = MODE_SIMPLE_INFO;
  @override
  Widget build(BuildContext context) {
    var world = widget.world;
    List<Widget> widgets = [];
    widgets.add(
      new Container (
        decoration:  new BoxDecoration(
          borderRadius: new BorderRadius.all(new Radius.circular(10.0)),
          gradient: new LinearGradient(
            colors: [
              Colors.green.shade700,
              Colors.transparent
            ],
            begin: Alignment.topLeft,
            end: Alignment.bottomLeft,
            stops: [0.0, 0.7]
          ),
        ),        
        padding: new EdgeInsets.all(8.0),
        child:  new ListTile (
          title: new Row (children: <Widget>[
            new Container(
              width: 100.0,
              height: 100.0,
              padding: new EdgeInsets.only(top: 4.0, bottom: 4.0),
              decoration: new BoxDecoration(
                shape: BoxShape.circle,
                color: const Color(0xff7c94b6),
                image: new DecorationImage(              
                  image: new ExactAssetImage(Common.globes[ world.name.hashCode % Common.globes.length ]),
                  fit: BoxFit.cover,
                ),
                border: new Border.all(
                  color: Colors.black,
                  width: 4.0,
                ),
              ),
            ),        
            new Expanded(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                  new Text (
                    world.name, style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 32.0
                    ),
                  )
                ],),
            ),
            ],
          ),
          onTap: () {     
            setState( () => showMode = (showMode +1) % (world.animals.isNotEmpty ?  3 : 2) );   
          }
        )
      )
    );
    widgets.add(new SWNWorldDataWidget(
      world
    ));
    if (world.relation != null) {
      widgets.add(new SWNRelationWidget(world.relation));
    }

    switch (showMode) {
      case MODE_SIMPLE_INFO:
        widgets.add(
          new Container (
            padding: new EdgeInsets.all(16.0),
            child:  Common.expandedBox(
              new Text (
                "${world.tag1.title}, ${world.tag2.title}",
                textAlign: TextAlign.left            
              ),
            ),
          )
        );
        break;
      case MODE_TAGS:
        widgets.add(new SWNTagWidget (
          world.tag1
        ));
        widgets.add(new SWNTagWidget (
          world.tag2
        ));
        break;
      case MODE_ANIMALS:
        if (world.animals.isNotEmpty) {
          List<Widget> subWidgets = [];
          subWidgets.add(new SWNAnimalListWidget(world.animals));
          widgets.add(new Container (
            padding: new EdgeInsets.all(4.0),
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: subWidgets,
            ),
          ));
        }
        break;
      }

    List<Widget> buttonRow = [];
    buttonRow.add (
      new Container (
        padding: new EdgeInsets.fromLTRB(0.0, 6.0, 0.0, 6.0),
        alignment: Alignment.bottomRight,
        child: new IconButton (
          splashColor: Colors.green.shade100,
          icon: new Icon(Icons.edit),
          onPressed: (){
            editWorldDialog(widget.world, context)
            .then( (data) {
              if (data != null) {
                print (data);
                setState( () {
                  world.name = data.name;
                  world.atmosphere = data.atmosphere;
                  world.biosphere = data.biosphere;
                  if (data.biosphere == 'No native biosphere')
                    world.animals.clear();
                  world.climate = data.climate;
                  world.population = data.population;
                  world.technology = data.technology;
                  world.tag1 = swnTagsMap[data.tag1];
                  world.tag2 = swnTagsMap[data.tag2];
                  if (data.hasRelation && world.relation != null) {
                    world.relation.contactPoint = data.contactPoint;
                    world.relation.origin = data.origin;
                    world.relation.currentRelation = data.currentRelation;
                  }
                });
              }
            });
          }),
        )      
    );
    buttonRow.add (
      new Expanded(
        child:  new Text(""),
      )
    );
    if (widget.showRemove)
      buttonRow.add (
        new Container (
          padding: new EdgeInsets.fromLTRB(0.0, 6.0, 0.0, 6.0),
          alignment: Alignment.bottomRight,
          child: new IconButton (
            splashColor: Colors.green.shade100,
            //child: new Text("Remove world from system"),
            icon: new Icon(Icons.delete),
            onPressed: (){
              if (widget.onRemoveButtonPressed != null)
                widget.onRemoveButtonPressed(widget.world);
            }),
          )      
      );
    widgets.add(new Row (
      children: buttonRow
      )
    );
    return new Container (
      padding : const EdgeInsets.all(2.0),
      child: new Card(
        child:  new Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisSize: MainAxisSize.min,
          children: widgets,
          ),
        ),
      );
    }
}
