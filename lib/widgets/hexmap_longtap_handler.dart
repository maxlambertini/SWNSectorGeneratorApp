/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import '../swn.dart';
import 'dart:async';

enum LongTapOptions {
  NewSystem,
  DeleteSystem,
  AddWorld,
  AddSituation,
  RenameSystem
}

class HexmapLongtapHandler {
  BuildContext ctx;
  SWNSector sector;
  SWNCoord coord;
  HexmapLongtapHandler(this.ctx, this.sector, this.coord);

  Future<LongTapOptions> handleLongTapWithCoord () async  {
    List<Widget> choices = [];
    choices.add(
      new SimpleDialogOption(
        onPressed: () { Navigator.pop(ctx, LongTapOptions.NewSystem); },
        child: new Row (
          children: <Widget>[
            new Padding(
              padding: new EdgeInsets.all(6.0),
              child:  new Icon(Icons.add_circle),
            ),
            new Text('Create or replace system'),              
          ],
        ),
      ),
    );
    if (sector.systems.containsKey(coord)) {
      choices.add(
        new SimpleDialogOption(
            onPressed: () { Navigator.pop(ctx, LongTapOptions.AddWorld); },
            child: new Row (
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(6.0),
                  child:  new Icon(Icons.add_circle),
                ),
                new Text('Add a new world'),              
              ],
            ),
          )
      );      
      choices.add(
        new SimpleDialogOption(
            onPressed: () { Navigator.pop(ctx, LongTapOptions.AddSituation); },
            child: new Row (
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(6.0),
                  child:  new Icon(Icons.add_circle),
                ),
                new Text('Add a new situation'),              
              ],
            ),
          )
      );      
      choices.add(
        new SimpleDialogOption(
            onPressed: () { Navigator.pop(ctx, LongTapOptions.DeleteSystem); },
            child: new Row (
              children: <Widget>[
                new Padding(
                  padding: new EdgeInsets.all(6.0),
                  child:  new Icon(Icons.delete_sweep),
                ),
                new Text('Delete current system'),              
              ],
            ),
          )
      );      
    }
    return await showDialog<LongTapOptions>(
      context: ctx,
      child: new SimpleDialog(
        title: const Text('Select operation'),
        children: choices,
      ),
    );
  }  
}