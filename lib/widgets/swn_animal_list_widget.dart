import 'package:flutter/material.dart';
import '../swn.dart';
import 'swn_animal_widget.dart';
import 'swn_common_data.dart' as Common;

class SWNAnimalListWidget extends StatefulWidget {
  final List<SWNAnimal> animalList;
  SWNAnimalListWidget(this.animalList);
  @override
  State<StatefulWidget> createState() {
      // TODO: implement createState
      return new SWNAnimalListWidgetState();
    }
}

class SWNAnimalListWidgetState extends State<SWNAnimalListWidget> {
  SWNAnimal currentAnimal;
  
  @override
  void initState() {
      // TODO: implement initState
      super.initState();
      currentAnimal = widget.animalList.first;
    }

  @override
  Widget build(BuildContext context) {
      // TODO: implement build
      return new Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: <Widget>[
          _buildDropDown(),
          new SWNAnimalWidget(currentAnimal),
        ],
      );
    }

  Widget _buildDropDown() {
    List<DropdownMenuItem<SWNAnimal>> menuItems = widget.animalList.map( (a) {
      return new DropdownMenuItem<SWNAnimal>(
        value: a,
        child: new Text (a.name)

      );
    }).toList();
    return new Row (
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        new Container(
          width: 64.0,
          height: 64.0,
          padding: new EdgeInsets.only(top: 4.0, bottom: 4.0, right:16.0),
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
            color: const Color(0xff7c94b6),
            image: new DecorationImage(              
              image: new ExactAssetImage(Common.animals[currentAnimal.name.hashCode % Common.animals.length ]),
              fit: BoxFit.cover,
            ),
            border: new Border.all(
              color: Colors.black,
              width: 4.0,
            ),
          ),
        ),
        new Container (width: 16.0, height:64.0),        
        new Expanded (
          child: new DropdownButton<SWNAnimal>(
          value: currentAnimal,
          items: menuItems,
          onChanged: (SWNAnimal newValue) {
            setState( () {
              currentAnimal = newValue;
            });
          },
        ),
        )
      ],
    );
  }
}