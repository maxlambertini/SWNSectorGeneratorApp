/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';
import 'swn_dialogs.dart';
import 'swn_situation.dart';
import 'swn_single_world_widget.dart';

class SWNSystemDetail extends StatefulWidget {
  final SWNSystem system;
  SWNSystemDetail(this.system);
  @override 
  createState() => new SWNSystemDetailState();
}


class SWNSystemDetailState extends State<SWNSystemDetail> {
  static String routeName = "SystemDetail";
  SWNSystem system;
  @override
  Widget build(BuildContext ctx) {
    system = widget.system;
    List<Widget> systemItems = [];
    system.worlds.forEach( (w) => systemItems.add( 
      new SWNSingleWorldWidget(
        world: w,
        showRemove: system.worlds.length > 1,
        onRemoveButtonPressed: (w) {
          confirmationDialog("Remove world from system?", ctx)
          .then( (b) {
            if (b) setState(() {
              system.worlds.remove(w);
              if (system.worlds.length == 1)
                system.worlds.first.relation = null;
              Scaffold.of(ctx).showSnackBar(
                new SnackBar(
                    backgroundColor: Colors.green.shade900,
                  content: new Text("World '${w.name}' removed"),
                )
              );
            });
          });
        },
      ) 
    ));
    system.situations.forEach( (s) => systemItems.add( 
      new SWNSituationWidget(
        situation: s,
        onRemoveSituationPressed: (sit) {
          confirmationDialog("Remove situation from system?", ctx)
          .then( (b) {
            if (b) setState((){
              //print ("Removing $sit");
              system.situations.remove(sit);
              Scaffold.of(ctx).showSnackBar(
                new SnackBar(
                  backgroundColor: Colors.green.shade900,
                  content: new Text("Situation '${sit.location}' removed"),
                )
              );
            });
          });
        },
      )
    ));
    var systemDetail =  new Column (
      children: <Widget>[
        new Expanded (
          child: new SingleChildScrollView (        
            child: new Column(
              children: systemItems
            ),
          ),
        ),
        new Row (children: <Widget> [
          new Expanded (
            child: new IconButton(
              icon: new Icon(Icons.add_circle),
              onPressed: () {
                setState( ()  {
                  if (widget.system.worlds.length < 4) {
                    var w = new SWNWorld.randomize();
                    w.relation = new SWNRelation.randomize(); 
                    widget.system.addWorld(w);
                    Scaffold.of(ctx).showSnackBar(
                      new SnackBar(
                        backgroundColor: Colors.green.shade900,
                        content: new Text("Added world '${w.name}' to current system"),
                      )
                    );
                  }
                });
              }),
            ),
          new Expanded (
            child: new IconButton(
              icon: new Icon(Icons.add_box),
              onPressed: () {
                setState( ()  {
                  if (widget.system.situations.length < 6)
                    widget.system.situations.add(new SWNSituation());
                    Scaffold.of(ctx).showSnackBar(
                      new SnackBar(
                        backgroundColor: Colors.green.shade900,
                        content: new Text("Added situation  to current system"),
                      )
                    );
                });
              }),
            ), 
           ] 
          ),
        ]
      );
    return systemDetail;
  }
}

