/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';
import 'dart:async';
import 'swn_editworld.dart';


Future<SWNEditWorldData> editWorldDialog(SWNWorld world, BuildContext ctx) async {
  SWNEditWorldData res = await Navigator.of(ctx).push (
    new MaterialPageRoute<SWNEditWorldData>(
      fullscreenDialog: true,
      builder:  (context) => new Scaffold(
        appBar: new AppBar(
          title: new Text (
            "Edit ${world.name}",
            style:  new TextStyle(
              fontFamily: "Exo",
              fontWeight: FontWeight.w900,
            ),
          )
        ),
        body: new Container (
          padding: new EdgeInsets.all(10.0),
          child: new SWNWorldEditor( 
            world: world,
            onCancelPressed: () {
              Navigator.pop(ctx, null);
            },
            onOkPressed: (worldData) {
              Navigator.pop(ctx, worldData);
            },
          ),
        ),
      )
    )
  );
  return res;
}

Future<String> inputBox (String message, String inputDefault, BuildContext ctx) async {
  TextEditingController tc = new TextEditingController();
  if (inputDefault != null)
    tc.text = inputDefault;
  return await showDialog<String> (
    context: ctx,
    child : new SimpleDialog (
      title: new Text(message),
      children: <Widget>[
        new Container (
            padding: new EdgeInsets.all(10.0),
            child: new Column(
              children: <Widget>[
                new Container(
                  padding: new EdgeInsets.only(top: 6.0, bottom: 6.0),
                  child: new TextField(
                    controller: tc, 
                  )
                ),
                new Row (children: <Widget>[
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(5.0),
                      child: new RaisedButton(
                        child: new Text("Ok"),
                        onPressed: () {
                          Navigator.pop(ctx,tc.text);
                        },
                      ),
                    )
                  ),
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(5.0),
                      child: new RaisedButton(
                        child: new Text("Cancel"),
                        onPressed: () {
                          Navigator.pop(ctx,null);
                        },
                      ),
                    )
                  ),
                ],
                )
              ],
            ),
        )
      ],
    )
  );
}


Future<bool> confirmationDialog (String message, BuildContext ctx) async {
  return await showDialog<bool> (
    context: ctx,
    child : new SimpleDialog (
      title: new Text(message),
      children: <Widget>[
        new Container (
            padding: new EdgeInsets.all(10.0),
            child: new Column(
              children: <Widget>[
                new Row (children: <Widget>[
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(5.0),
                      child: new RaisedButton(
                        child: new Text("Yes"),
                        onPressed: () {
                          Navigator.pop(ctx,true);
                        },
                      ),
                    )
                  ),
                  new Expanded(
                    child: new Container(
                      padding: new EdgeInsets.all(5.0),
                      child: new RaisedButton(
                        child: new Text("No"),
                        onPressed: () {
                          Navigator.pop(ctx,false);
                        },
                      ),
                    )
                  ),
                ],
                )
              ],
            ),
        )
      ],
    )
  );
}

