/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import '../swn.dart';
import 'swn_dialogs.dart';
import 'swn_situation.dart';
import 'swn_single_world_widget.dart';

class SWNSystemCard extends StatefulWidget {

  SWNSystemCard(this.system, this.coord);
  final SWNSystem system;
  final SWNCoord coord;
  @override
  createState() => new SWNSystemCardState(system,coord);
}


class SWNSystemCardState extends State<SWNSystemCard> {
  
  SWNSystem system;
  SWNCoord coord;
  
  SWNSystemCardState(this.system, this.coord) : super();

  @override
  Widget build (BuildContext context) {
    return new Container (
      padding : const EdgeInsets.all(2.0),
      child: new Card(
        child:  new Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            new ListTile(
              leading: const Icon(Icons.place),
              title: new Text(coord.toString()+" "+ system.worlds.first.name+ " [${system.worlds.length}, ${system.situations.length}]"),
              subtitle: new Text("${system.worlds.first.tag1.title}, ${system.worlds.first.tag2.title}"),
              onTap: () {
                _showDetail(system, context);
              }
            ),
          ],
        ),
      ),
    );
  }

  void  _showDetail(SWNSystem system, BuildContext ctx) {
    List<Widget> systemItems = [];
    system.worlds.forEach( (w) => systemItems.add( 
      new SWNSingleWorldWidget(
        world: w,
        showRemove: system.worlds.length > 1,
        onRemoveButtonPressed: (wrld) {
          confirmationDialog("Remove world from system?", ctx)
          .then( (b) {
            if (b) {
              setState((){
                system.worlds.remove(wrld);
                if (system.worlds.length == 1)
                  system.worlds.first.relation = null;
                Scaffold.of(ctx).showSnackBar(
                  new SnackBar(
                    backgroundColor: Colors.green.shade900,
                    content: new Text("World '${wrld.name}' removed"),
                  )
                );
              });
            }
          });
        },
      ) 
    ));
    system.situations.forEach( (s) => systemItems.add( new SWNSituationWidget(situation: s)));
    Navigator.of(ctx).push (    
      new MaterialPageRoute (
        builder: (ctx) {
          var systemDetail =  new SingleChildScrollView (        
            child: new Column(
              children: systemItems
            ),
          );  
          return new Scaffold(
            appBar: new AppBar(
              title: new Text ("Details of ${system.worlds.first.name}"),
            ),
            body: systemDetail
          );
        } 
      )
    );
  }

}


List<SWNSystemCard> sectorCards(SWNSector sector) {
  var data = sector.systems.keys.map( (coord) => new SWNSystemCard(sector.systems[coord], coord));
  return data.toList();
}

