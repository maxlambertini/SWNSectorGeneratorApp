/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';

var animals = [
    'assets/animals/01.jpg'
    ,'assets/animals/02.jpg'
    ,'assets/animals/03.jpg'
    ,'assets/animals/04.jpg'
    ,'assets/animals/05.jpg'

];

var globes = [
    'assets/arid_planet_2.jpg'
    ,'assets/arid_planet.jpg'
    ,'assets/cold_planet.jpg'
    ,'assets/earthlike_cold.jpg'
    ,'assets/earthlike_cold_2.jpg'
    ,'assets/earthlike_planet.jpg'
    ,'assets/hostile_planet.jpg'
    ,'assets/hostile_planet_2.jpg'
    ,'assets/hot_planet.jpg'
    ,'assets/jungle_planet.jpg'
    ,'assets/weird_planet.jpg'
    ,'assets/planet-blue.jpg'
    ,'assets/planet-cyan.jpg'
    ,'assets/planet-yellow.jpg'
    ,'assets/planet-blue.jpg'
    ,'assets/planet_01.jpg'
    ,'assets/planet_02.jpg'
    ,'assets/planet_03.jpg'
    ,'assets/planet_04.jpg'
    ,'assets/planet_05.jpg'
] ;

Widget expandedBox (Widget w) {
    return new Row (children: <Widget>[
      new Expanded(
        child: new Container(
          child: w,
          padding: new EdgeInsets.fromLTRB(0.0, 8.0,.0,4.0),
        )
      )
    ],
    );
  }

