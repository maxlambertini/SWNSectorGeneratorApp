import 'package:flutter/material.dart';

class SectionItem {
  String head;
  String value;
  SectionItem(this.head, this.value);
}

class SectionItemWidget extends StatelessWidget {
  final String title;
  final List<SectionItem> sectionItems;
  final bool useDivider;
  final bool showTitle;
  final TextStyle titleStyle;
  final TextStyle headStyle;
  final TextStyle valueStyle;
  final EdgeInsets margin;
  SectionItemWidget(this.sectionItems, 
    {
    this.title, 
    this.useDivider = true,
    this.showTitle  = true,
    this.titleStyle = const TextStyle(fontSize: 18.0, fontWeight: FontWeight.bold),
    this.headStyle = const TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold),
    this.valueStyle  = const TextStyle(fontSize: 12.0, fontWeight: FontWeight.normal),
    this.margin = const EdgeInsets.all(10.0)
  });

  @override
  Widget build(BuildContext context) {
      // TODO: implement build
      List<Widget> widgets = [];
      if (showTitle) {
        widgets.add(new Text( title, style: titleStyle, ));
        widgets.add(new Divider());
      }
      sectionItems.forEach( (item) {
        widgets.add(new Text (item.head, style: headStyle, ));
        widgets.add(new Text ( item.value,  style: valueStyle,));
        if (useDivider) widgets.add(new Divider());
      });
      return new Container ( 
          child: new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          
          children: widgets, 
        ),
        margin: this.margin
      );
    }

}