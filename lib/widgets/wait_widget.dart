/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';

class WaitWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp (
      title: "SWN Sector Builder",
      theme: new ThemeData(
        fontFamily: "Encode",
        brightness: Brightness.dark,
        primaryColor: Colors.green.shade900,
        accentColor: Colors.greenAccent
      ),
      home: new Scaffold (
        body: new Center (
          child: new Text ("Please Wait....",
            style:  new TextStyle(
              fontFamily: "Exo",
              fontSize: 24.0,
              fontWeight: FontWeight.w900,
            ),
          ),
        ),
      ),
    );
  }
}