/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/services.dart';
import 'dart:async';
import 'dart:io';

const CHANNEL="org.lamboz.www/interfaces";

const platform = const MethodChannel(CHANNEL);

Future<String> getFilesDir() async {
  String path = await platform.invokeMethod("getFilesDir");
  return path;
}

Future<String> shareFile(String sectorName, String filePath) async {
  String result = await platform.invokeMethod("shareSvgSector", {"sectorName" : sectorName, "sectorFile": filePath});
  return result;
}


Future<String> getApplicationMainPath() async {
  String basePath = await getFilesDir();
  basePath = basePath +"/SWNMobileSectors";
  Directory dir = new Directory(basePath);
  bool dirExists = await dir.exists();
  if (!dirExists)
    await dir.create();
  return basePath;
}

Future<String> getApplicationJsonPath() async {
  String basePath = await getApplicationMainPath();
  basePath = basePath +"/json";
  Directory dir = new Directory(basePath);
  bool dirExists = await dir.exists();
  if (!dirExists)
    await dir.create();
  return basePath;
}

Future<Directory> getApplicationJsonPathDir() async {
  String basePath = await getApplicationMainPath();
  basePath = basePath +"/json";
  Directory dir = new Directory(basePath);
  bool dirExists = await dir.exists();
  if (!dirExists)
    await dir.create();
  return dir;
}

Future<String> getApplicationHtmlPath() async {
  String basePath = await getApplicationMainPath();
  basePath = basePath +"/html";
  Directory dir = new Directory(basePath);
  bool dirExists = await dir.exists();
  if (!dirExists)
    await dir.create();
  return basePath;
}

Future<Directory> getApplicationHtmlPathDir() async {
  String basePath = await getApplicationMainPath();
  basePath = basePath +"/html";
  Directory dir = new Directory(basePath);
  bool dirExists = await dir.exists();
  if (!dirExists)
    await dir.create();
  return dir;
}