/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:yaml/yaml.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:async' show Future;
import 'dart:convert';


Future<List<String>> getListOfPlaces() async {
  List<String> res = [];
  String data = await rootBundle.loadString("assets/datafiles/names.txt");
  data.split("\\n").forEach( (line) => res.addAll(line.split("/")));
  print ("done places");
  return res;
}

Future<dynamic> loadSituations() async {
  String data = await rootBundle.loadString("assets/datafiles/situations.yaml");
  print ("done sits");
  return loadYaml(data);
}

Future<dynamic> loadRelations() async {
  String data = await rootBundle.loadString("assets/datafiles/relations.yaml");
  print ("done rel");
  return loadYaml(data);
}

Future<dynamic> loadWorldTables() async {
  String data = await rootBundle.loadString("assets/datafiles/worldtables.yaml");
  print ("done world tables");
  return loadYaml(data);
}

Future<dynamic> loadJsonTags() async {
  String data = await rootBundle.loadString("assets/datafiles/tags.json");
  print ("done tags");
  return jsonDecode(data);
}


List<String> listOfPlaces = [];
dynamic docSwnSituations;
var docSwnWorldTables;
var docSwnWorldTags;
var docWorldRelations;
var tagList ;

Future<Null> initializeListOfPlaces() async {
  print ("Loading places....");
  listOfPlaces = await getListOfPlaces();
  print ("Loading loadSituations....");
  docSwnSituations = await loadSituations();
  print ("Loading loadWorldTables....");
  docSwnWorldTables = await loadWorldTables();
  print ("Loading loadJsonTags....");
  docSwnWorldTags = await loadJsonTags();
  print ("Loading loadRelations....");
  docWorldRelations = await loadRelations();
  print ("Loading docSwnWorldTags....");
  tagList = docSwnWorldTags["Tags"].map ((t) => t["Title"]).toList();
  print ("stuff loaded, exiting in an async way");
}


List<String> getTableLabels(String table) {
  List<String> res = docSwnWorldTables[table].map( (s) => s["Description"]).toList().cast<String>();
  List<String> res2 = [];
  res.forEach( (f){
    res2.add(f.toString());
  });
  return res;
}



