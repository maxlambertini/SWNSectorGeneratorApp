/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'swn_random.dart';
import 'swn_data.dart'  as SWN;
import 'swn_nomen.dart' as Nomen;
import 'dart:convert';
import 'dart:async';
import 'dart:io';
import 'package:markdown/markdown.dart';
import 'swn_animals.dart';

Nomen.NameBuilder nameBuilder = new Nomen.NameBuilder();

int id_counter=1000;

class SWNRelation {
  String origin;
  String currentRelation;
  String contactPoint;
  SWNRelation.randomize() {
    var idx1 = rnd.nextInt(8);
    var idx2 = rnd.nextInt(8);
    var idx3 = rnd.nextInt(8);
    var doc = SWN.docWorldRelations["Relations"];
    origin = doc[idx1]["Origin"];
    currentRelation = doc[idx2]["CurrentRelation"];
    contactPoint = doc[idx3]["ContactPoint"];
  }
  @override
  //toString() => "Relation";
  toString() => "* Origin: $origin\n* Current Relation: $currentRelation\n* Contact Point $contactPoint\n";
  toHtml() => """
  <h4 class="relation-title-western">Relation with primary:</h4>
  <ul class='ul-relation-western'>
  <li class="relation-item-western"><b>Origin:</b> $origin</li>
  <li class="relation-item-western"><b>Current relation:</b> $currentRelation</li>
  <li class="relation-item-western"><b>Contact point:</b> $contactPoint</li>
  </ul>
  """;

  Map<String, dynamic> toMap() => {
    'origin' : origin,
    'currentRelation' : currentRelation,
    'contactPoint' : contactPoint
  };

  SWNRelation.fromMap(dynamic map) {
    origin = map['origin'];
    currentRelation = map['currentRelation'];
    contactPoint = map['contactPoint'];
  }

}

class SWNTag {
  String title;
  String description;
  String enemies;
  String friends;
  String complications;
  String places;

  SWNTag( this.title, this.description, this.enemies, this.friends, this.complications, this.places);
  SWNTag.fromDynamic(dynamic d) {
    description = d["Description"];
    title = d["Title"];
    enemies = d["Enemies"];
    friends = d["Friends"];
    complications = d["Complications"];
    places = d["Places"];
  }
  @override
  String toString() {
    return "$title\n\n$description\n* Enemies: ${enemies}* Friends:${friends}* Complications: ${complications}* Places ${places}";
  }

  toHtml() => """
    <h4 class="tag-title-western">$title</h4>
    <p class="tag-description-western">$description</p>
    <ul class="tag-list-western">
      <li class="tag-item-western"><b>Enemies:</b> $enemies</li>
      <li class="tag-item-western"><b>Friends:</b> $friends</li>
      <li class="tag-item-western"><b>Complications:</b> $complications</li>
      <li class="tag-item-western"><b>Places:</b> $places</li>
    </ul>
  """;

  Map<String, dynamic> toMap() => {
    'description' : description,
    'title' : title,
    'enemies' : enemies,
    'friends' : friends,
    'complications' : complications,
    'places' : places
  };

  SWNTag.fromMap(dynamic d) {
    description = d["description"];
    title = d["title"];
    enemies = d["enemies"];
    friends = d["friends"];
    complications = d["complications"];
    places = d["places"];
  }
  
}


class SWNWorld {
  String atmosphere;
  String biosphere;
  String climate;
  String population;
  String technology;
  SWNTag tag1;
  SWNTag tag2;
  SWNRelation relation;
  List<SWNAnimal> animals = [];
  String name;
  int climateIdx = 0;
  int atmosphereIdx = 0;
  int biosphereIdx = 0;


  int habitabilityIdx() =>((climateIdx+atmosphereIdx+biosphereIdx) / 3).truncate()-2;

  SWNWorld(this.name, this.atmosphere, this.biosphere, this.climate, this.population, this.tag1, this.tag2);
  SWNWorld.randomize() {
    name = nameBuilder.createName();
    atmosphereIdx = d6()+d6();
    atmosphere = getTableIdx("Atmosphere",atmosphereIdx);
    
    biosphereIdx = d6()+ d6();
    biosphere = getTableIdx("Biosphere",biosphereIdx);

    int nAnimal = 0;
    if (! (biosphereIdx >= 4 && biosphereIdx <= 5)) {
      nAnimal = 1+d6();
      for (int h = 0; h < nAnimal; h++)
      { 
        SWNAnimal animal = new SWNAnimalBuilder().buildAnimal();
        animals.add(animal);
      }
    }

    climateIdx = d6()+d6();
    climate = getTableIdx("Climate",climateIdx );
    population = getTable("Population");
    technology = getTable("Technology");
    tag1 = getTag();
    tag2 = getTag();
    while (tag1.title == tag2.title)
      tag2 = getTag();
  }

  @override
  toString() {
    var s =  "## $name\n\n* Atmosphere: **$atmosphere**\n* Biosphere: **$biosphere**\n* Climate: **$climate**\n* Population: **$population**\n* Technology: **$technology**\n\n### $tag1 \n\n### $tag2";
    if (this.relation != null) {
      s += "\n\n### Relations with primary: \n\n$relation";
    }
    return s;
  }

  toHtml() {
    String relation = this.relation != null ? this.relation.toHtml() : "";

    String animals = "";
    if (this.animals.isNotEmpty) {
      StringBuffer sb = new StringBuffer();
      sb.writeln("<div id='atitle_${id_counter++}' ><h3 class='animal-title-western'>Notable animals:</h3></div>");
      sb.writeln("<div dir='ltr' gutter='32' style='column-count:2;' id='animals_${id_counter++}'>");
      for (var a in this.animals) {
        sb.writeln(a.toHtml());
      }
      sb.writeln('</div>');
      animals = sb.toString();
    }

    String wName = this.name.replaceAll(" ", "");
    return """
    <div class="div-world-western" id="world_${id_counter++}">
    <h3 class='world-title-western'>$name</h3>
    </div>
    <div class="div-tags-western" dir="ltr" gutter="32" style="column-count:2;" id="world_desc_${id_counter++}">
    <table class='conditions-western'  style='margin-top:12pt; margin-bottom:12pt; border: none; padding:0.01cm;'  style='margin-top:8pt;margin-bottom:8pt;'>
      <tr valign='top' ><th style='border: none; padding:0.01cm;'  >Atmosphere</th><td style='border: none; padding:0.01cm;'  >$atmosphere</td></tr>
      <tr valign='top' ><th style='border: none; padding:0.01cm;'  >Biosphere</th><td style='border: none; padding:0.01cm;'  >$biosphere</td></tr>
      <tr valign='top' ><th style='border: none; padding:0.01cm;'  >Climate</th><td style='border: none; padding:0.01cm;'  >$climate</td></tr>
      <tr valign='top' ><th style='border: none; padding:0.01cm;'  >Population</th><td style='border: none; padding:0.01cm;'  >$population</td></tr>
      <tr valign='top' ><th style='border: none; padding:0.01cm;'  >Technology</th><td style='border: none; padding:0.01cm;'  >$technology</td></tr>
    </table>
    <br />
    ${tag1.toHtml()}
    ${tag2.toHtml()}
    <br />
    $relation
    </div>

    $animals
    """;
  }

  Map<String,dynamic> toMap() { 
    var m ={
      'atmosphere': atmosphere,
      'biosphere': biosphere,
      'climate':  climate,
      'population': population,
      'technology' : technology,
      'climateIdx' : climateIdx,
      'atmosphereIdx' : atmosphereIdx,
      'biosphereIdx' : biosphereIdx,
      'name' : name,
      'tag1' : tag1.toMap(),
      'tag2' : tag2.toMap()
    };
    if (animals.length != 0)
      m['animals'] = animals.map( (an) => an.toMap() ).toList();
    else 
      m['animals'] = [];
    return m;
  }

  SWNWorld.fromMap(dynamic d) {
    atmosphere = d['atmosphere'];
    biosphere = d['biosphere'];
    climate = d['climate'];
    population = d['population'];
    technology = d['technology'];
    climateIdx = d['climateIdx'];
    atmosphereIdx = d['atmosphereIdx'];
    biosphereIdx = d['biosphereIdx'];
    name = d['name'];
    tag1 = new SWNTag.fromMap(d['tag1']);
    tag2 = new SWNTag.fromMap(d['tag2']);
    List<dynamic> oAnimals = d['animals'];
    if (oAnimals.length != 0)
      animals = oAnimals.map( (d) => new SWNAnimal.fromMap(d)).toList().cast<SWNAnimal>();
  }
}

class SWNCoord implements Comparable<SWNCoord>{
  int x;
  int y;
  SWNCoord(this.x, this.y);
  SWNCoord.randomize() {
    x = rnd.nextInt(8);
    y = rnd.nextInt(10);
  }
  @override
  operator == (dynamic other) => this.y == other.y && this.x == other.x;
  
  operator < (SWNCoord other) {
    if (this.y < other.y)
      return true;
    else if (this.y == other.y && this.x < other.x)
      return true;
    else
      return false;
  }
  operator > (SWNCoord other){
    if (this.y > other.y)
      return true;
    else if (this.y == other.y && this.x > other.x)
      return true;
    else
      return false;
  }
  int get hashCode => 17+37*this.y.hashCode+37*this.x.hashCode;

  int compareTo (SWNCoord other) {
    int res = 0; 
    if (this < other)
      res = -1;
    else if (this > other)
      res = 1;
    else res = 0;
    return res;
  }

  @override
  toString() => "[$x, $y]";
  Map<String, dynamic> toMap() => { 'x' : x, 'y': y};
  SWNCoord.fromMap(dynamic d) {
    x = d['x'];
    y = d['y'];
  }
}

class SWNSystem {

  List<SWNWorld> worlds = [];
  List<SWNSituation> situations = [];
  String name;
  SWNSystem.randomize() {
    name = nameBuilder.createName();
    worlds.add(new SWNWorld.randomize());
    if (rnd.nextInt(7)==4) {
      var w2 =new SWNWorld.randomize();
      w2.relation = new SWNRelation.randomize();
      worlds.add(w2);
    }
    if (d8() > 4) {
      int sits = d6() -2 ;
      if (sits < 1 ) sits = 1;
      for (int h = 0; h < sits; h++) 
        situations.add(new SWNSituation());
    }
  } 
  SWNSystem(SWNWorld w) {
    worlds.add(w);
  } 
  void addWorld(SWNWorld w) {
    worlds.add(w);
  }

  toHtml({SWNCoord coord}) {
    int h = 1;
    StringBuffer buf = new StringBuffer();
    buf.writeln("<div class='system' id='system_${id_counter++}'><h2>$coord $name</h2></div>");
    for (var w in worlds) {
      buf.writeln(w.toHtml());
      h++;
    }
    if (situations.length > 0) {
      buf.writeln("<div class='situations-western' id='sit_list_${id_counter++}'><h3 class='situation-title-western'>Situations</h3></div>");
      for (var s in situations)
        buf.writeln(s.toHtml()); 
      buf.writeln('</div>');
    }
    return buf.toString();
  }
  

  @override toString() {
    int h = 1;
    StringBuffer buf = new StringBuffer();
    buf.writeln("##  $name");
    for (SWNWorld w in worlds) {
      buf.writeln("\n$w\n\n");
      h++;
    }
    if (situations.length > 0) {
      for (var s in situations)
        buf.writeln("$s\n"); 
    }
    return buf.toString();
  }

  Map<String, dynamic> toMap() => {
    'name' : name,
    'worlds' : worlds.map( (m) => m.toMap()).toList(),
    'situations' : situations.map( (s) => s.toMap()).toList()
  };

  SWNSystem.fromMap (dynamic d) {
    name = d['name'];
    List<dynamic> wList = d['worlds'];
    wList.forEach( (w) => worlds.add(new SWNWorld.fromMap(w)));
    wList = d['situations'];
    wList.forEach( (w) => situations.add(new SWNSituation.fromMap(w)));
  }  
}

class SWNSector {
  
  Map<SWNCoord, SWNSystem> systems = {};
  String name = nameBuilder.createName();

  Map<String, dynamic> toMap() => {
    'name' : name,
    'systems' : systems.keys.map( (k) => {
      'coord': k.toMap(),
      'system': systems[k].toMap()
    }).toList()
  };

  String toJson() =>jsonEncode(toMap()); 

  Future<Null> toJsonFile(String filePath) async {
      await new File(filePath).writeAsString(this.toJson());
  }

  Future<Null> toHtmlFile(String filePath) async {
      await new File(filePath).writeAsString(toHtml());
  }

  SWNSector.fromMap (dynamic d) {
    name = d['name'];
    List<dynamic> ldSys = d['systems'];
    ldSys.forEach( (s) {
      SWNCoord c = new SWNCoord.fromMap(s['coord']);
      SWNSystem sys = new SWNSystem.fromMap(s['system']);
      systems[c] = sys;
    }); 
  }


  SWNSector.fromJson(String json)  {
    dynamic d = jsonDecode(json);
    name = d['name'];
    List<dynamic> ldSys = d['systems'];
    ldSys.forEach( (s) {
      SWNCoord c = new SWNCoord.fromMap(s['coord']);
      SWNSystem sys = new SWNSystem.fromMap(s['system']);
      systems[c] = sys;
    }); 
  }

  static Future<SWNSector> fromJsonFile(String filePath) async {
    String json = await  new File(filePath).readAsString();
    print ("Json loaded... length ${json.length}");
    SWNSector sec = new SWNSector.fromJson(json);
    return sec;
  }

  

  SWNSector.cloneFromSector(SWNSector old) {
    this.name = old.name;
    old.systems.keys.forEach( (k) {
      this.systems[k] = old.systems[k];
    });
  }

  SWNSector() {
    int nSectors = 14 + d8() +d8();
    for (int h = 0; h < nSectors; h++) {
      SWNCoord coord = new SWNCoord.randomize();
      if (!this.systems.containsKey(coord)) {
        this.systems[coord] = new SWNSystem.randomize();
      }
      else {
        coord = new SWNCoord.randomize();
        while (this.systems.containsKey(coord))
          coord = new SWNCoord.randomize();
        this.systems[coord] = new SWNSystem.randomize();
      }
    }
  }

  void addSystem (SWNCoord coord) {
    systems[coord] = new SWNSystem.randomize();
  }

  void addWorld (SWNCoord coord) {
    if (systems.containsKey(coord)) {
      var w = new SWNWorld.randomize();
      w.relation = new SWNRelation.randomize(); 
      systems[coord].addWorld(w); 
    }
  }

  void addSituation (SWNCoord coord) {
    if (systems.containsKey(coord)) { 
      if (systems[coord].situations.length < 6)
        systems[coord].situations.add(new SWNSituation());
    }
  }

  void removeSystem (SWNCoord coord) {
    if (systems.containsKey(coord)) 
      systems.remove(coord);
  }

  String drawHex (double x, double y, double sideSize) {
    SWNCoord coord = new SWNCoord(x.toInt(), y.toInt());
    double deltaY = sideSize * 0.5;
    double xStep = x*sideSize *1.1;
    double yStep = y*sideSize *1.1;
    double stepY = (x.toInt() % 2 == 0) ? 0.0 : deltaY; 
    double radius = sideSize * 0.2;
    double radius2 = sideSize * 0.1;
    var x0 = xStep;                                         var y0 = yStep+stepY;
    var x1 = xStep+ sideSize;                               var y1 = yStep+stepY;
    var x2 = xStep+ sideSize;                               var y2 = yStep+sideSize+stepY;
    var x3 = xStep;                                         var y3 = yStep+sideSize+stepY;
    
    var xText = xStep + sideSize * 0.05;
    var yText = yStep + sideSize +stepY -(sideSize * 0.05);
    
    double cx = x0 + sideSize/2.0;
    double cy = y0 + sideSize/2.0 ;
    double textHeight = sideSize / 10.0;
    String sRes =  '<path d="M $x0 $y0 L $x1 $y1 $x2 $y2 $x3 $y3 $x0 $y0" stroke="#AAAAAA" fill="white" stroke-width="{$sideSize*0.05}"/>\n';
    sRes += '<text x="$xText" y="${yText}" fill="#AAAAAA" font-family="sans-serif" font-size="$textHeight">$coord</text>\n';
    if (this.systems.containsKey(coord)) {
      SWNSystem sys = this.systems[coord];
      sRes +="<!-- world -->\n";
      sRes += '<circle cx="$cx" cy="$cy" r="$radius" fill="#DDDDDD" stroke="#AAAAAA" stroke-width="${sideSize*0.02}" />\n';
      if (sys.worlds.length > 1) {
        sRes += '<circle cx="$cx" cy="${cy+radius2}" r="$radius2" fill="#AAAAAA" stroke="#555555" stroke-width="${sideSize*0.02}" />\n';
        
      }

      //now draw situations! :-) 
      var tinySize = sideSize / 9.0;
      var tinyStep = tinySize * 1.3;
      var tinyRadius = tinySize / 2.0;

      for (int h = 0; h < sys.situations.length; h++) {
        final xc = (tinyStep+x0)+tinyStep*h.toDouble();
        final yc = y0+tinyStep;
        sRes += '<circle cx="$xc" cy="$yc" r="$tinyRadius" fill="#AAAAAA" stroke="#888888" stroke-width="${sideSize*0.01}" />\n';
      }

      
      sRes +='<text x="$cx" y="${cy+ radius + textHeight *1.2}" text-anchor="middle" font-family="sans-serif" font-size="$textHeight">${sys.name}</text>\n';

    }

    return sRes;

  }

  String getSectorMap() {
    StringBuffer buf = new StringBuffer('''<?xml version="1.0" encoding="UTF-8"?>
    <svg  xmlns="http://www.w3.org/2000/svg"  xmlns:xlink="http://www.w3.org/1999/xlink"  
    width="3560" height="4710" viewbox="-10 -15 3550 4700" >''');
    for (double y = 0.0; y < 10.0; y+= 1.0) {
      for (double x = 0.0; x < 8.0; x+= 1.0) {
        buf.writeln(drawHex(x, y, 400.0));
      }
    }
    buf.writeln('</svg>');
    return buf.toString();
  }

  String getImgInlineData() {
    String svg = getSectorMap();
    var bytes =  utf8.encode(svg);
    var base64 = base64Encode(bytes);
    return "data:image/svg+xml;base64,"+base64;
  }

  @override toString() {
    StringBuffer buf = new StringBuffer();
    buf.writeln("<div class='sector'>");
    buf.writeln("<h1 class='sector-title-western'>Sector: $name</h1>\n\n");
    buf.writeln("\n\n<div class='sectormap'>");
    //buf.writeln(getSectorMap());
    buf.writeln("<img style='width:16cm;height:21.2cm;' alt='Sector: $name' src='${getImgInlineData()}' />");
    buf.writeln("</div>\n\n");
    this.systems.forEach( (k,v) => buf.writeln(markdownToHtml(v.toString())) );
    buf.writeln("</div>\n\n");
    return buf.toString();
  }

  _toHtml() {
    StringBuffer buf = new StringBuffer();
    buf.writeln("<h1>Sector: $name</h1>\n\n");
    buf.writeln("\n\n<div class='sectormap'>");
    //buf.writeln(getSectorMap());
    buf.writeln("<img style='width:16cm;height:21.2cm;' alt='Sector: $name' src='${getImgInlineData()}' />");
    buf.writeln("</div>\n\n");
    List<SWNCoord> coords = systems.keys.toList();
    coords.sort();
    coords.forEach( (k) => buf.writeln( systems[k].toHtml(coord: k)));
    return buf.toString();
  }



  toHtml() {
    var bareHtml =  this._toHtml();
    var sectorTable = this.toSectorTable();
    id_counter = 1000;
    return """<!DOCTYPE html>
    <html lang="en-EN">
    <head>
      <title>Sector $name</title>
      <meta charset="UTF-8" />
	<meta name="generator" content="LibreOffice 5.4.5.1 (Windows)"/>
    <style type="text/css">
BODY {font-family: "Constantia", serif; font-size: 11pt; } 
      @page { margin-left: 1.5cm; margin-right: 2cm; margin-top: 1.5cm; margin-bottom: 2cm; }
      p { font-family: "Constantia", serif; font-size: 11pt; }
      p.western { font-family: "Constantia", serif; font-size: 11pt; }
      p.cjk { font-size: 11pt; }
      p.ctl { font-size: 11pt ;}
      td p { direction: inherit }
      td p { font-family: "Constantia"; font-size: 8pt; margin: 0; }
      td p.western { font-family: "Constantia"; font-size: 8pt }
      td p.cjk { font-size: 8pt; }
      td p.ctl { font-size: 8pt; }
      h1 { font-family: "Candara"; }
      h1.western { font-family: "Candara"; }
      h1.cjk { font-size: 24pt; }
      h1.ctl { font-size: 24pt ;}
      h2 { font-family: "Candara"; font-size: 16pt ;margin-top: 0.42cm; border-top: none; border-bottom: 1px dotted #000000; border-left: none; border-right: none; padding-top: 0cm; padding-bottom: 0.05cm; padding-left: 0cm; padding-right: 0cm; direction: inherit; page-break-before: always }
      h2.western { font-family: "Candara"; font-size: 16pt; }
      h2.cjk { font-family: "SimSun"; font-size: 16pt ;}
      h2.ctl { font-family: "Mangal"; font-size: 16pt ;}
      h3 { font-family: "Candara"; margin-top: 0.28cm; margin-bottom: 0.14cm; direction: inherit }
      h3.western { font-family: "Candara" }
      h3.cjk { font-family: "SimSun" }
      h3.ctl { font-family: "Mangal" }
      h4 {font-family: "Constantia", serif; font-style: normal; margin-bottom: 0.07cm; direction: inherit }
      h4.western { font-family: "Constantia", serif; font-style: normal }
      h4.cjk { font-family: "SimSun"; font-style: italic; font-weight: normal }
      h4.ctl { font-family: "Mangal"; font-style: italic; font-weight: normal }
      th p { font-family: "Constantia"; font-size: 11pt; margin: 0; }
      th p.western { font-family: "Constantia"; font-size: 11pt; }
      th p.cjk { font-size: 11pt }
      th p.ctl { font-size: 11pt }
      td p { margin: 0pt; padding: 0pt;}
      th p { margin: 0pt; padding: 0pt; }
      th {padding: 0pt; }
      td {padding: 0pt; }
      td p.sys-header { font-size: 10pt;  font-weight: bold; }
      td p.sys-header-western { font-size: 10pt;  font-weight: bold; }
      td p.swn-sysname {  margin-bottom: 0cm; font-size: 9pt; }
      td p.swn-planetname {  margin-bottom: 0cm; font-size: 9pt; font-weight:bold; }
      td p.swn-planetinfo {  margin-bottom: 0cm; font-size: 9pt; }
      td p.swn-planettags {  margin-bottom: 0cm; font-size: 9pt; }
      td p.swn-sit_location {  margin-bottom: 0cm; font-size: 9pt; font-weight:bold;}
      td p.swn-situation  {  margin-bottom: 0cm; font-size: 9pt; }
      td p.swn-sysname-western {  margin-bottom: 0cm; font-size: 9pt ; }
      td p.swn-planetname-western {  margin-bottom: 0cm; font-size: 9pt; font-weight: bold; }
      td p.swn-planetinfo-western {  margin-bottom: 0cm; font-size: 9pt; }
      td p.swn-planettags-western {  margin-bottom: 0cm; font-size: 9pt;}
      td p.swn-sit_location-western {  margin-bottom: 0cm; font-size: 9pt; font-weight:bold;}
      td p.swn-situation-western  {  margin-bottom: 0cm; font-size: 9pt }
      td p {
        margin-bottom: 0cm;
      }
      td p.western {
        margin-bottom: 0cm;
      }
      h3.animal-title-western { margin-top:0.2in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; font-size: 11pt; }
    	h4.animal-subtitle-western { margin-top:0.1in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; margin-bottom: 0.0in; font-size: 9pt; }
      p.animal-style-western { margin-top:0.0in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; font-size: 8pt; font-weight:bold;}
      p.look-western { margin-top:0.0in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; font-size: 8pt; font-style: italic;}
      p.behavior-western { margin-top:0.0in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; font-size: 8pt; }
      p.attack-western { margin-top:0.0in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; padding:0.1in;background-color: #FFAA00; font-size: 8pt; }
      p.poison-western { margin-top:0.0in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; padding:0.1in; background-color: #AAFFAA; font-size: 8pt; }
      h3.animal-title { margin-top:0.2in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; font-size: 11pt; }
    	h4.animal-subtitle { margin-top:0.1in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; margin-bottom: 0.0in; font-size: 9pt; }
      p.animal-style { margin-top:0.0in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; font-size: 8pt; font-weight:bold;}
      p.look { margin-top:0.0in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; font-size: 8pt; font-style: italic;}
      p.behavior { margin-top:0.0in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; font-size: 8pt; }
      p.attack { margin-top:0.0in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; padding:0.1in;background-color: #FFAA00; font-size: 8pt; }
      p.poison { margin-top:0.0in; margin-bottom: 0.0in; margin-left: 0.0in; margin-right: 0.0in; padding:0.1in; background-color: #AAFFAA; font-size: 8pt; }
      
          
    </style>
    </head>
    <body>
    $bareHtml

    <h1 class='summary-main-title-western'>Summary of sector $name</h1>
    $sectorTable

    </body>
    """;
    

  }

  // terse table sector showing: 
  // - name,
  // - atmosphere
  // - biosphere
  // - climate
  // - tech
  // - population
  // - tags
  String toSectorTable() {

    var sysMap = new Map<String, dynamic>(); 
    for (final k in systems.keys) {
      final s = systems[k];
      sysMap[s.name] = {"System" : s, "Coord" : k};
    }
    StringBuffer buf = new StringBuffer();
    var keys = sysMap.keys.toList();
    keys.sort();
    for (final k in keys) {
      final SWNSystem sys = sysMap[k]["System"];
      final SWNCoord coord = sysMap[k]["Coord"];
      buf.writeln("<tr bgcolor='#EEEEEE' valign='top'  class='sys-row-western'><td colspan='5' style='background-color: #EEEEEE; border: none; padding:0cm'  class='sys-header-western'><p class='sys-header-western'>$coord - ${sys.name}</p></td></tr>");
      for (final w in sys.worlds) {
        buf.writeln("""<tr valign='top'  class='world-row-western'>
        <td style='border: none; padding:0cm'  ><p class='swn-planetname-western'>${w.name}</p></td>
        <td style='border: none; padding:0cm'   colspan="3"<p class='swn-planetinfo-western'>${w.atmosphere}, ${w.biosphere}, ${w.climate}, ${w.population}, ${w.technology}</p></td>
        <td style='border: none; padding:0cm'  ><p class='swn-planettags-western'>${w.tag1.title}, ${w.tag2.title}</p></td>
        </tr>""");
      }
      for (final sit in sys.situations) {
        buf.writeln("""<tr valign='top'  class='situation-row'>
          <td style='border: none; padding:0cm'   colspan="1"><p class='swn-sit_location-western'>${sit.location}</p></td>
          <td style='border: none; padding:0cm;'   colspan='4'><p class='swn-situation-western'>Occupied by: ${sit.occupiedBy} on situation: ${sit.situation}</p></td>
          </tr>""");
      }
    }    
    return "<table id='sector-summary' style='border: none; padding:0.01cm;' >${buf.toString()}</table>";
  }
}


class SWNSituation {
  String location;
  String situation;
  String occupiedBy;
  SWNSituation() {
    var s = getSituation();
    location = s["Location"];
    situation = s["Situation"];
    occupiedBy = s["Occupied By"];
  }
  @override toString() => "#### $location\n\n* Situation: **$situation**\n* Occupied by: **$occupiedBy**\n";

  @override toHtml() => """
  <h4 class='situation-title-western'>$location</h4>
  <p class='sitbody-western'>
  Occupied by $occupiedBy. Situation: $situation
  </p>
  """;

  Map<String, dynamic> toMap() => {
    'location' : location,
    'situation' : situation,
    'occupiedBy' : occupiedBy
  };

  SWNSituation.fromMap(dynamic map) {
    location = map['location'];
    situation = map['situation'];
    occupiedBy = map['occupiedBy'];
  }
  
}

SWNTag getTag() => new SWNTag.fromDynamic( swnTags[rnd.nextInt(swnTags.length)]);

Map<String,SWNTag> getMapTags() {
  var map = new Map<String, SWNTag>();
  for ( var t in swnTags) {
    var tag = new SWNTag.fromDynamic(t);
    map[tag.title] = tag;
  }
  return map;
}

final swnTagsMap = getMapTags();


void main() {
  SWNSector s1 = new SWNSector();
  print (s1.toHtml());
  s1.toHtmlFile(s1.name+".html").then( (s) {
    print ("Saved to $s");
  });
}