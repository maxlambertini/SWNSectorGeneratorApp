/* This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/. */

import 'package:flutter/material.dart';
import 'swn.dart';
import 'widgets/wait_widget.dart';
import 'widgets/swn_main_scaffold_hexmap.dart';

void main()  {
  runApp(new SWNApp());
} 

typedef Widget BuildWidgetDelegate(BuildContext ctx);


class SWNApp extends StatefulWidget {
  
  SWNApp():super();
  @override 
  createState() => new SWNAppState();

}

class SWNAppState extends State<SWNApp> {
  SWNAppState():super();


  @override
  void initState() {
    print ("Initalizing...");
    super.initState();
    print ("Setting up waiting widget.");
    _component = _waitWidget;
    print ("Done setting waiting widget.");
    initializeListOfPlaces()
    .then( (_){
      print ("Creating sector."); 
      sector = new SWNSector();
      setState( () {
        print ("Setting up component.");        
        _component = _mainWidget;
        print ("Set up main widget...");
        print (new SWNAnimalBuilder().buildAnimal());

      });
    });

  }

  BuildContext contextInner;
  BuildWidgetDelegate _component ;

  SWNSector sector;
  final GlobalKey<ScaffoldState> scaffoldKey = new GlobalKey<ScaffoldState>();

  Widget _waitWidget(BuildContext context) {
    return new WaitWidget();
  }

  Widget _mainWidget(BuildContext context) {
    return new MaterialApp(
      title: "SWN Sector Builder",
      theme: new ThemeData(
        fontFamily: "Encode",
        brightness: Brightness.dark,
        primaryColor: Colors.green.shade900,
        accentColor: Colors.greenAccent
      ),
      home: new SWNMainScaffoldHexmap(
        sector: this.sector
      ), 
    );
  }
  
  @override
  Widget build(BuildContext context) {
    return _component(context);
  }

}