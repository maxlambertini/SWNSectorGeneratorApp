# SWN Mobile Sector

This is a Sector Generator for the fine sci-fi roleplaying game [Stars Without Numbers](http://www.drivethrurpg.com/product/226996/Stars-Without-Number-Revised-Edition). If you're into sci-fi roleplaying
game and love OSR games, you should **definitely** check it out. Even if you, like me, are into a more story game-y style (say FATE, Apocalypse World and the like) check it out nevertheless, because it is chock full of stuff that 
can be mined for serious gaming fun. 

## Installing the app

This app has yet to be published in Google Play app store. However, you can get a binary apk here:

http://www.mediafire.com/file/rm5fhz64mlklbjt/swn-mobile-sectors.apk

If you want to build it from source, clone this repo and it's a matter of running `flutter run` to test it and `flutter build apk; flutter install`

## Instructions

The program is roughly modeled upon [Sectors Without Number](https://sectorswithoutnumber.com/), which is a nifty
sector generation app for the web users. Due to my laziness :-) I've chosen to draw the hexmap using staggered squares
instead of plain hexes.

App's quite straightforward to use: if you want to check/edit a system, tap on the square containing one; if you want to delete a system from a square, or create a new one in an empty square, long press on the chosen square. Double-tapping on the map resets zoom factor and position. 

When in system detail screen, click on a planet name to expand Tags in their entirety, and click on the planet name again to hide them. 

Once you're happy with your sector tinkering, you can save the sector in your local storage or generate an html file 
that can be safely opened with word processors like Libre Office Writer and provide the basis for a campaign or a sourcebook :-) 

### Screenshots

Here are the obligatory screenshots:

<img src="./screenshots/01.png" width="400" style="width:400px;" />
<img src="./screenshots/02.png" width="400" style="width:400px;" />
<img src="./screenshots/03.png" width="400" style="width:400px;" />


## Features for the budding Flutter developer

I belong to this category too, but IMHO there are some noteworthy features demonstrated in this program:

- A complete, custom-drawn widget with panning and pinch zoom.
- How create custom widget that capture events from their subwidget and bubble them up.
- Custom channels to exploit OS stuff. 
- How to use custom fonts.

## Compatibility

App's been tested on an Android Emulator configured with Android 5.0 and 512kb ram. Tested with Kitkat 
gave problems -- app would start but splash screen would freeze. So, if you're in the lower 19% of Android 
user -- sorry. 

Since I have neither a mac nor an iphone, iOS is a totally unsupported OS. Moreover, to share stuff and 
write data I had to write custom channels, so if you're a brave IOS dev be warned that you'll be somewhat on 
your own. 

## License

This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/. 
